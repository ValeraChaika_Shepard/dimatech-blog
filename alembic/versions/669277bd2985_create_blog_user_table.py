"""create blog_user table

Revision ID: 669277bd2985
Revises: 
Create Date: 2021-07-24 21:56:13.762698

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '669277bd2985'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'blog_user',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('login', sa.String(20), unique=True),
        sa.Column('password', sa.String(255)),
    )


def downgrade():
    op.drop_table('blog_user')
