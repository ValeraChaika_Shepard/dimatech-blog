"""create article table

Revision ID: a07028dc32c3
Revises: 669277bd2985
Create Date: 2021-07-24 21:59:31.813044

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a07028dc32c3'
down_revision = '669277bd2985'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'article',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('title', sa.String(100)),
        sa.Column('announcement', sa.String(255)),
        sa.Column('text', sa.Text),
        sa.Column('publication_date', sa.Date),

        # author_id
        sa.Column('author_id', sa.Integer, sa.ForeignKey('blog_user.id')),

    )


def downgrade():
    op.drop_table('article')
