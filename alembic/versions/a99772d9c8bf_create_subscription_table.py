"""create subscription table

Revision ID: a99772d9c8bf
Revises: a07028dc32c3
Create Date: 2021-07-24 22:03:51.622874

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a99772d9c8bf'
down_revision = 'a07028dc32c3'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'subscription',
        sa.Column('id', sa.Integer, primary_key=True),

        #user_id
        sa.Column('user_id', sa.Integer, sa.ForeignKey('blog_user.id')),
        #subscribed_to_id
        sa.Column('subscribed_to_id', sa.Integer, sa.ForeignKey('blog_user.id')),
    )


def downgrade():
    op.drop_table('subscription')
