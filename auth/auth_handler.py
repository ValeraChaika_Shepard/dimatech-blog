import jwt
import datetime
from sanic import Blueprint
from hashlib import sha256
from models.blog_user import BlogUser
from sanic.response import json, text
from sqlalchemy import select
from asyncpg.exceptions import UniqueViolationError
from sqlalchemy.exc import IntegrityError
from sqlalchemy.exc import DBAPIError

from utils.validation_request_args import schema_login, schema_registration
#from sanic_validation import validate_args
from sanic_validation import validate_json


auth = Blueprint("auth", url_prefix="/auth")


#Из-за validation_request_args.py можно эту функцию убрать
def check_field(field):
    """Проверка поля на пустоту"""
    if field == None or field.strip() == "":
        return False
    return True


@auth.post("/registration")
#@validate_args(schema_registration)
@validate_json(schema_registration)
async def registration(request):
    """Регистрации пользователя"""
    session = request.ctx.session
    async with session.begin():
        # login_value = request.form.get("login")
        # password_value = request.form.get("password")
        # confirm_password_value = request.form.get("confirm_password")

        login_value = request.json["login"]
        password_value = request.json["password"]
        confirm_password_value = request.json["confirm_password"]

        log_check = check_field(login_value)
        pass_check = check_field(password_value)
        conf_pass_check = check_field(confirm_password_value)

        stmt = select(BlogUser).where(BlogUser.login == login_value)
        result = await session.execute(stmt)
        user = result.first()

        blog_user = None

        if log_check and pass_check and conf_pass_check and password_value == confirm_password_value and user == None:
            try:
                blog_user = BlogUser(login=login_value, password=sha256(password_value.encode()).hexdigest())
                session.add_all([blog_user])
            except UniqueViolationError:
                return json({"error": "This login is busy, try new login"})
            except IntegrityError:
                return json({"error": "This login is busy, try new login"})
            except DBAPIError:
                return json({"error": "This login is busy, try new login"})
            except Exception:
                return json({"error": "This login is busy, try new login"})
        else:
            return json({"error": "You input unvalid data to registration or login is busy"})
    return json({"ok": "New user was created", "blog_user": blog_user.to_dict()}, status=201)


@auth.post("/login")
#@validate_args(schema_login)
@validate_json(schema_login)
async def login(request):
    """Авторизация пользователя"""
    session = request.ctx.session
    async with session.begin():
        token = None

        #будто из формы взял данные
        #login_value = request.form.get("login")
        #password_value = request.form.get("password")

        login_value = request.json["login"]
        password_value = request.json["password"]

        log_check = check_field(login_value)
        pass_check = check_field(password_value)

        blog_user = None

        if log_check and pass_check:
            stmt = select(BlogUser).where(BlogUser.login == login_value).where(BlogUser.password == sha256(password_value.encode()).hexdigest())
            result = await session.execute(stmt)
            user = result.first()

            if user:
                user_id = user._asdict()['BlogUser'].id
                user_login = user._asdict()['BlogUser'].login

                payload = {"user_id" : user_id, "login": user_login, "exp": datetime.datetime.utcnow() + datetime.timedelta(minutes=30)}

                token = jwt.encode(payload, request.app.config.SECRET)
                #token = jwt.encode(payload, request.app.config.SECRET)
                return json({"user" : user._asdict()['BlogUser'].to_dict(), "token" : token})
            else:
                return json({"error" : "User not found or incorrect login/password"})
        else:
            return json({"error" : "You input unvalid data to login"})