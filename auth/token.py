import jwt
from functools import wraps
from sanic.response import json, text


def check_token(request):
    """Проверка наличия токена в запросе"""
    if not request.token:
        return False
    try:
        jwt.decode(request.token, request.app.config.SECRET, algorithms=["HS256"])['user_id']
    except jwt.exceptions.InvalidTokenError:
        return False
    except jwt.ExpiredSignatureError:
        return json({"error" : "Timeout of token"})
    except KeyError:
        return False
    else:
        return True


def jwt_protected(wrapped):
    """Данная функция создает декоратор для проверки jwt-токена"""
    def decorator(f):
        @wraps(f)
        async def decorated_function(request, *args, **kwargs):
            is_authenticated = check_token(request)
            if is_authenticated:
                response = await f(request, *args, **kwargs)
                return response
            else:
                return json({"error" : "You are unauthorized"}, status = 401)
        return decorated_function
    return decorator(wrapped)