--
-- PostgreSQL database dump
--

-- Dumped from database version 13.3
-- Dumped by pg_dump version 13.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: article; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.article (
    id bigint NOT NULL,
    title character varying(100) NOT NULL,
    announcement character varying(255) NOT NULL,
    text text NOT NULL,
    publication_date date NOT NULL,
    author_id bigint
);


ALTER TABLE public.article OWNER TO postgres;

--
-- Name: article_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.article_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.article_id_seq OWNER TO postgres;

--
-- Name: article_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.article_id_seq OWNED BY public.article.id;


--
-- Name: blog_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.blog_user (
    id bigint NOT NULL,
    login character varying(20) NOT NULL,
    password character varying(255) NOT NULL
);


ALTER TABLE public.blog_user OWNER TO postgres;

--
-- Name: blog_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.blog_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.blog_user_id_seq OWNER TO postgres;

--
-- Name: blog_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.blog_user_id_seq OWNED BY public.blog_user.id;


--
-- Name: subscription; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.subscription (
    id bigint NOT NULL,
    user_id bigint,
    subscribed_to_id bigint
);


ALTER TABLE public.subscription OWNER TO postgres;

--
-- Name: subscription_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.subscription_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.subscription_id_seq OWNER TO postgres;

--
-- Name: subscription_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.subscription_id_seq OWNED BY public.subscription.id;


--
-- Name: article id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.article ALTER COLUMN id SET DEFAULT nextval('public.article_id_seq'::regclass);


--
-- Name: blog_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_user ALTER COLUMN id SET DEFAULT nextval('public.blog_user_id_seq'::regclass);


--
-- Name: subscription id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subscription ALTER COLUMN id SET DEFAULT nextval('public.subscription_id_seq'::regclass);


--
-- Data for Name: article; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.article (id, title, announcement, text, publication_date, author_id) FROM stdin;
1	‘в вмп1	 ­­®в жЁп1	вҐЄбв1_1	2021-07-14	1
2	‘в вмп2	 ­­®в жЁп2	вҐЄбв1_2	2021-07-10	1
3	‘в вмп3	 ­­®в жЁп3	вҐЄбв1_3	2021-07-11	1
4	‘в вмп21	 ­­®в жЁп2_1	вҐЄбв2_1	2021-07-14	2
5	‘в вмп22	 ­­®в жЁп2_2	вҐЄбв2_2	2021-01-28	2
6	‘в вмп31	 ­­®в жЁп3_1	вҐЄбв3_1	2021-05-30	3
7	‘в вмп41	 ­­®в жЁп4_1	вҐЄбв4_1	2020-12-31	4
8	‘в вмп42	 ­­®в жЁп4_2	вҐЄбв4_2	2021-02-01	4
9	‘в вмп43	 ­­®в жЁп4_3	вҐЄбв4_3	2021-06-06	4
10	‘в вмп44	 ­­®в жЁп4_4	вҐЄбв4_4	2021-07-10	4
11	‘в вмп51	 ­­®в жЁп5_1	вҐЄбв5_1	2021-07-13	5
12	‘в вмп52	 ­­®в жЁп5_2	вҐЄбв5_2	2021-07-08	5
\.


--
-- Data for Name: blog_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.blog_user (id, login, password) FROM stdin;
1	Valera	9f735e0df9a1ddc702bf0a1a7b83033f9f7153a00c29de82cedadc9957289b05
2	Max78	9f735e0df9a1ddc702bf0a1a7b83033f9f7153a00c29de82cedadc9957289b05
3	vicKYSIK_best	9f735e0df9a1ddc702bf0a1a7b83033f9f7153a00c29de82cedadc9957289b05
4	Dima$322	9f735e0df9a1ddc702bf0a1a7b83033f9f7153a00c29de82cedadc9957289b05
5	BorisBBL05	9f735e0df9a1ddc702bf0a1a7b83033f9f7153a00c29de82cedadc9957289b05
\.


--
-- Data for Name: subscription; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.subscription (id, user_id, subscribed_to_id) FROM stdin;
1	1	2
2	1	5
3	2	1
4	2	3
5	2	4
6	3	4
7	4	1
8	4	5
9	5	1
10	5	2
11	5	3
12	5	4
\.


--
-- Name: article_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.article_id_seq', 12, true);


--
-- Name: blog_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.blog_user_id_seq', 5, true);


--
-- Name: subscription_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.subscription_id_seq', 12, true);


--
-- Name: article article_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.article
    ADD CONSTRAINT article_pkey PRIMARY KEY (id);


--
-- Name: blog_user blog_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_user
    ADD CONSTRAINT blog_user_pkey PRIMARY KEY (id);


--
-- Name: subscription subscription_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subscription
    ADD CONSTRAINT subscription_pkey PRIMARY KEY (id);


--
-- Name: blog_user unique_login; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blog_user
    ADD CONSTRAINT unique_login UNIQUE (login);


--
-- Name: article article_author_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.article
    ADD CONSTRAINT article_author_id_fkey FOREIGN KEY (author_id) REFERENCES public.blog_user(id);


--
-- Name: subscription subscription_subscribed_to_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subscription
    ADD CONSTRAINT subscription_subscribed_to_id_fkey FOREIGN KEY (subscribed_to_id) REFERENCES public.blog_user(id);


--
-- Name: subscription subscription_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subscription
    ADD CONSTRAINT subscription_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.blog_user(id);


--
-- PostgreSQL database dump complete
--

