from sqlalchemy.orm import sessionmaker
from models.blog_user import BlogUser
from sqlalchemy import select
from hashlib import sha256
from sanic.response import json
from middlewares.setting import bind
from sqlalchemy.ext.asyncio import AsyncSession
from random import randint

async def create_2_users(app, loop):
    session = sessionmaker(bind, AsyncSession, expire_on_commit=False)()
    session.begin()

    counter = 0

    while counter != 2:
        login_value = "test" + str(randint(100, 10000))
        password_value = "testpassword"

        stmt = select(BlogUser).where(BlogUser.login == login_value)
        result = await session.execute(stmt)
        user = result.first()

        if user == None:
            blog_user = BlogUser(login=login_value, password=sha256(password_value.encode()).hexdigest())
            session.add(blog_user)
            counter += 1
        else:
            continue

    await session.commit()
    await session.close()