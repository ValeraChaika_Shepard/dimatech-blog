from contextvars import ContextVar
from sqlalchemy.ext.asyncio import create_async_engine
from server import app

_base_model_session_ctx = ContextVar("session")

bind = create_async_engine(
  	f"postgresql+asyncpg://{app.config.DB_USER}:{app.config.DB_USER_PASS}@{app.config.DB_HOST}/{app.config.DB_NAME}",
  	echo=True)