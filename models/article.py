from sqlalchemy import Integer, Column, ForeignKey, String, Text, Date
from sqlalchemy.orm import declarative_base, relationship
from models.base_model import BaseModel


class Article(BaseModel):
    """Модель для связи с таблицей article"""
    __tablename__ = "article"

    title = Column(String(100))
    announcement = Column(String(255))
    text = Column(Text())
    publication_date = Column(Date())

    author_id = Column(Integer, ForeignKey("blog_user.id"))

    author = relationship("BlogUser", back_populates="articles")

    def to_dict(self):
        return {"id": self.id,
         "title": self.title,
         "announcement": self.announcement,
         "text": self.text,
         "publication_date": str(self.publication_date),
         "author_id": self.author_id}