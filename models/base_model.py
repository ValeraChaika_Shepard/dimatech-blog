from sqlalchemy import Integer, Column, ForeignKey, String, Text, Date
from sqlalchemy.orm import declarative_base, relationship


Base = declarative_base()


class BaseModel(Base):
    """Базовая абстрактная модель"""
    __abstract__ = True
    id = Column(Integer, primary_key=True)