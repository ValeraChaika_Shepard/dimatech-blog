from sqlalchemy import Integer, Column, ForeignKey, String, Text, Date
from sqlalchemy.orm import declarative_base, relationship
from models.base_model import BaseModel


class BlogUser(BaseModel):
    """Модель для связи с таблицей blog_user"""
    __tablename__ = "blog_user"

    login = Column(String(20), unique=True)
    password = Column(String(255))

    articles = relationship("Article")

    subscriptions = relationship("Subscription", foreign_keys="Subscription.user_id")
    subscribed_to_me = relationship("Subscription", foreign_keys="Subscription.subscribed_to_id")

    def to_dict(self):
        return {"id": self.id, "login": self.login}