from sqlalchemy import Integer, Column, ForeignKey, String, Text, Date
from sqlalchemy.orm import declarative_base, relationship
from models.base_model import BaseModel


class Subscription(BaseModel):
    """Модель для связи с таблицей subscription"""
    __tablename__ = "subscription"

    user_id = Column(Integer, ForeignKey("blog_user.id"))
    subscribed_to_id = Column(Integer, ForeignKey("blog_user.id"))

    from_user = relationship("BlogUser", foreign_keys="Subscription.user_id", back_populates="subscriptions")
    to_user = relationship("BlogUser", foreign_keys="Subscription.subscribed_to_id", back_populates="subscribed_to_me")

    def to_dict(self):
        return {
         "id": self.id,
         "user_id": self.user_id,
         "subscribed_to_id": self.subscribed_to_id}