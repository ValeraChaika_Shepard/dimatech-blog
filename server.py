from sanic import Sanic
from settings import db_settings
from auth.auth_handler import auth
import urls


"""Настройка приложения"""
app = Sanic("Dimatech blog")

app.config.update(db_settings)

app.config.SECRET = "KEEP_IT_SECRET_KEEP_IT_SAFE"
app.blueprint(auth)

from middlewares.request import inject_session
from middlewares.response import close_session

app.register_middleware(inject_session, "request")
app.register_middleware(close_session, "response")

urls.set_routs(app)

from listeners.before_server_start import create_2_users

app.register_listener(create_2_users, "before_server_start")