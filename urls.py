import views.article as article
import views.blog_user as blog_user
import views.subscription as subscription


def set_routs(app):
    app.add_route(article.index, '/', methods=["GET"])
    app.add_route(article.create_article, '/article/create', methods=["POST"])
    app.add_route(article.get_author_articles, '/article/author/<id>', methods=["GET"])
    app.add_route(article.get_feed, '/feed', methods=["GET"])

    app.add_route(blog_user.get_all_users, '/users', methods=["GET"])

    app.add_route(subscription.subscribe_to_author, '/subscription/subscribe', methods=["POST"])
    app.add_route(subscription.unsubscribe_to_author, '/subscription/unsubscribe', methods=["DELETE"])