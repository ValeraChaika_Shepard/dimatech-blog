# login – логин создаваемого пользователя
# password – пароль создаваемого пользователя
# confirm_password – повторный ввод пароля, чтобы убедиться, что не была допущена ошибка
#
# title – наименование статьи
# announcement – анонс статьи
# text – текст статьи
#
# subscribed_to_id – уникальный идентификатор (УИ) автора, на которого необходимо подписаться/отписаться


schema_login = {
    'login': {
        'type': 'string',
        'required': True,
        'empty': False
    },
    'password': {
        'type': 'string',
        'required': True,
        'empty': False
    }
}


schema_registration = {
    'login': {
        'type': 'string',
        'required': True,
        'empty': False
    },
    'password': {
        'type': 'string',
        'required': True,
        'empty': False,
        'regex': '[A-Za-z0-9@#$%^&+=]{8,}'
    },
    'confirm_password': {
        'type': 'string',
        'required': True,
        'empty': False
    }
}


shema_create_article = {
    'title': {
        'type': 'string',
        'required': True,
        'empty': False
    },
    'announcement': {
        'type': 'string',
        'required': True,
        'empty': False
    },
    'text': {
        'type': 'string',
        'required': True,
        'empty': False
    }
}


shema_subscription = {
    'subscribed_to_id': {
        'type': 'integer',
        'required': True,
        'empty': False
    }
}