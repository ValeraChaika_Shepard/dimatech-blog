import jwt
from sqlalchemy import select, delete
from models.article import Article
from sanic.response import json, text
from auth.token import jwt_protected
from datetime import date, datetime
from models.subscription import Subscription

from utils.validation_request_args import shema_create_article
from sanic_validation import validate_args

from sanic_validation import validate_json


def get_current_user_id(request):
    """Получаем id авторизованного пользователя из его токена"""
    user_id = None
    try:
        user_id = jwt.decode(request.token, request.app.config.SECRET, algorithms=["HS256"])['user_id']
    except KeyError:
        return -1
    return user_id


async def index(request):
    """Возвращает 5 последних статей (сортировка по дате публикации)"""
    session = request.ctx.session
    async with session.begin():
        stmt = select(Article).limit(5).order_by(Article.publication_date.desc())
        result = await session.execute(stmt)
        articles = result.all()

    if not articles:
        return json({"error": "0 articles"})
    return json([{"article": article._asdict()['Article'].to_dict()} for article in articles])


#@validate_args(shema_create_article)
@validate_json(shema_create_article)
@jwt_protected
async def create_article(request):
    """Создаем статью"""
    session = request.ctx.session
    async with session.begin():
        # title_value = request.form.get("title")
        # announcement_value = request.form.get("announcement")
        # text_value = request.form.get("text")

        title_value = request.json["title"]
        announcement_value = request.json["announcement"]
        text_value = request.json["text"]

        today = date.today()
        publication_date_value = today.strftime("%Y-%m-%d")

        author_id_value = get_current_user_id(request)

        if author_id_value == -1:
            return json({"error" : "You are unauthorized"})

        article = Article(title = title_value,
        	announcement = announcement_value,
        	text = text_value,
        	publication_date = datetime.strptime(publication_date_value, '%Y-%m-%d'),
        	author_id = author_id_value)
        session.add_all([article])
    return json({"ok" : "New article was created", "article" : article.to_dict()}, status = 201)


@jwt_protected
async def get_author_articles(request, id):
    """Возвращает все статьи определенного автора"""
    session = request.ctx.session
    async with session.begin():
        stmt = select(Article).where(Article.author_id == int(id))
        result = await session.execute(stmt)
        articles = result.all()

    if not articles:
        return json({"error": f"There is no author in the system with id = {id} or he has no articles"})
    return json([{"article": article._asdict()['Article'].to_dict()} for article in articles])


@jwt_protected
async def get_feed(request):
    """Получаем все статьи тех авторов, на которых подписан пользователь"""
    session = request.ctx.session
    async with session.begin():
        user_id = get_current_user_id(request)

        if user_id == -1:
            return json({"error" : "You are unauthorized"}, status = 401)

        stmt = select(Subscription).where(Subscription.user_id == user_id)
        result = await session.execute(stmt)
        subscriptions = result.all()

        subscribed_id_list = []
        for subscription in subscriptions:
            subscribed_id_list.append(subscription._asdict()['Subscription'].subscribed_to_id)

        stmt = select(Article).where(Article.author_id.in_(subscribed_id_list))
        result = await session.execute(stmt)
        articles = result.all()

    if not articles:
        return json({"error": "Your subscribers do not have articles or you are not subscribed to anyone"})
    return json([{"article": article._asdict()['Article'].to_dict()} for article in articles])