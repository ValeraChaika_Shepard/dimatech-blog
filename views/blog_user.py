from models.blog_user import BlogUser
from sanic.response import json, text
from auth.token import jwt_protected
from sqlalchemy import select, delete

@jwt_protected
async def get_all_users(request):
    """Возвращает id и login всех пользователей системы"""
    session = request.ctx.session
    async with session.begin():
        stmt = select(BlogUser)
        result = await session.execute(stmt)
        blog_users = result.all()

    if not blog_users:
        return json({"error": "0 users"})
    return json([{"user": user._asdict()['BlogUser'].to_dict()} for user in blog_users])