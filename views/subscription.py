import jwt
from sanic.response import json, text
from sqlalchemy import select, delete
from auth.token import jwt_protected
from models.subscription import Subscription

from utils.validation_request_args import shema_subscription
from sanic_validation import validate_args

from sanic_validation import validate_json

def get_current_user_id(request):
    """Получаем id авторизованного пользователя из его токена"""
    user_id = None
    try:
        user_id = jwt.decode(request.token, request.app.config.SECRET, algorithms=["HS256"])['user_id']
    except KeyError:
        return -1
    return user_id


#@validate_args(shema_subscription)
@validate_json(shema_subscription)
@jwt_protected
async def subscribe_to_author(request):
    """Оформляем/Создаем подписку на определенного пользователя"""
    session = request.ctx.session
    async with session.begin():
        user_id_value = get_current_user_id(request)

        if user_id_value == -1:
            return json({"error": "You are unauthorized"}, status = 401)

        #subscribed_to_id_value = request.form.get("subscribed_to_id")

        subscribed_to_id_value = request.json["subscribed_to_id"]

        if int(user_id_value) == int(subscribed_to_id_value):
            return json({"error": "You can't subscribe to yourself"})

        subscription = Subscription(user_id=int(user_id_value),
                                    subscribed_to_id=int(subscribed_to_id_value))

        session.add_all([subscription])
    return json(
        {"ok": f"You subscribed to user with id {subscribed_to_id_value}", "subscription": subscription.to_dict()}, status = 201)


#@validate_args(shema_subscription)
@validate_json(shema_subscription)
@jwt_protected
async def unsubscribe_to_author(request):
    """Отменяем подписку на определенного пользователя"""
    session = request.ctx.session
    async with session.begin():
        user_id_value = get_current_user_id(request)

        if user_id_value == -1:
            return json({"error": "You are unauthorized"}, status = 401)

        #subscribed_to_id_value = request.args.get("subscribed_to_id")

        subscribed_to_id_value = request.json["subscribed_to_id"]

        if int(user_id_value) == int(subscribed_to_id_value):
            return json({"error": "You can't unsubscribe to yourself"})

        stmt = delete(Subscription).where(Subscription.user_id == int(user_id_value)).where(
            Subscription.subscribed_to_id == int(subscribed_to_id_value))
        result = await session.execute(stmt)
    return json({"ok": f"You unsubscribe from user with id = {subscribed_to_id_value}"})